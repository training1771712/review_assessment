import 'package:final_task/utils/custom_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'bloc/quiz_bloc.dart';
import 'bloc/quiz_event.dart';
import 'bloc/quiz_state.dart';

class QuestionPage extends StatefulWidget {
  const QuestionPage({Key? key}) : super(key: key);

  @override
  _QuestionPageState createState() => _QuestionPageState();
}

class _QuestionPageState extends State<QuestionPage> {
  int currentPageIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blueGrey[200],
      body: BlocBuilder<QuizBloc, QuizState>(
        builder: (context, state) {
          if (state is QuizLoading) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          } else if (state is QuizLoaded) {
            final questions = state.questions;
            final currentQuestion = questions[currentPageIndex];
            final questionText = currentQuestion['question'] ?? 'No question available';
            final questionType = currentQuestion['question_typeid'] as int;
            final options = currentQuestion['answers'] as List<dynamic>;

            String optionHeaderText = '';
            if (questionType == 1) {
              optionHeaderText = 'Select the most accurate option';
            } else if (questionType == 2) {
              optionHeaderText = 'Select all that apply';
            }

            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.only(left: 25, right: 25, top: 100, bottom: 50),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Center(
                        child: Text(
                          questionText,
                          style: const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Container(
                    margin: const EdgeInsets.symmetric(horizontal: 20),
                    color: Colors.white,
                    child: SingleChildScrollView(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Text(
                              optionHeaderText,
                              style: const TextStyle(fontSize: 18),
                            ),
                            ListView.builder(
                              shrinkWrap: true,
                              physics: const NeverScrollableScrollPhysics(),
                              itemCount: options.length,
                              itemBuilder: (context, index) {
                                final option = options[index];
                                final optionText = option['answer'] ?? 'No option available';

                                if (questionType == 1) {
                                  return Container(
                                    decoration: BoxDecoration(
                                      border: Border.all(color: Colors.grey),
                                    ),
                                    margin: const EdgeInsets.symmetric(vertical: 10, horizontal: 6),
                                    child: RadioListTile<int>(
                                      title: Text(optionText, style: const TextStyle(fontSize: 22)),
                                      activeColor: Colors.black,
                                      value: option['answer_id'] as int,
                                      groupValue: _getSelectedAnswerId(state),
                                      onChanged: (value) {
                                        _selectAnswer(value);
                                      },
                                    ),
                                  );
                                } else if (questionType == 2) {
                                  return Container(
                                    decoration: BoxDecoration(
                                      border: Border.all(color: Colors.grey),
                                    ),
                                    padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 8),
                                    margin: const EdgeInsets.symmetric(vertical: 10, horizontal: 6),
                                    child: Row(
                                      children: [
                                        Checkbox(
                                          value: _isSelectedAnswer(option['answer_id'], state),
                                          activeColor: Colors.black,
                                          onChanged: (value) {
                                            _selectAnswer(option['answer_id']);
                                          },
                                        ),
                                        const SizedBox(width: 10),
                                        Expanded(
                                          child: Text(
                                            optionText,
                                            style: const TextStyle(fontSize: 18),
                                          ),
                                        ),
                                      ],
                                    ),
                                  );
                                }
                                return const SizedBox.shrink();
                              },
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      CustomIconButton(
                        icon: Icons.navigate_before,
                        onPressed: currentPageIndex > 0 ? _navigateToPreviousQuestion : null,
                      ),
                      CustomIconButton(
                        icon: Icons.navigate_next,
                        onPressed: _isNextButtonEnabled(state) ? _navigateToNextQuestion : null,
                      ),
                    ],
                  ),
                ),
              ],
            );
          } else {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
    );
  }

  int? _getSelectedAnswerId(QuizLoaded state) {
    if (state.questions.length > currentPageIndex) {
      final selectedAnswers = state.questions[currentPageIndex]['selected_answers'] as List<dynamic>;
      if (selectedAnswers.isNotEmpty) {
        return selectedAnswers[0]['answer_id'] as int?;
      }
    }
    return null;
  }

  bool _isSelectedAnswer(int answerId, QuizLoaded state) {
    if (state.questions.length > currentPageIndex) {
      final selectedAnswers = state.questions[currentPageIndex]['selected_answers'] as List<dynamic>;
      return selectedAnswers.any((answer) => answer['answer_id'] == answerId);
    }
    return false;
  }

  void _selectAnswer(int? answerId) {
    final quizBloc = context.read<QuizBloc>();
    quizBloc.add(SetSelectedAnswer(answerId));
  }

  bool _isNextButtonEnabled(QuizLoaded state) {
    final questions = state.questions;
    if (questions.length > currentPageIndex) {
      final selectedAnswers = questions[currentPageIndex]['selected_answers'] as List<dynamic>;
      if (selectedAnswers.isNotEmpty) {
        final questionType = questions[currentPageIndex]['question_typeid'] as int;
        if (questionType == 1) {
          return true;
        } else if (questionType == 2) {
          return selectedAnswers.any((answer) => answer['is_selected']);
        }
      }
    }
    return false;
  }

  void _navigateToNextQuestion() {
    final quizBloc = context.read<QuizBloc>();
    quizBloc.add(NavigateToNext());
  }

  void _navigateToPreviousQuestion() {
    final quizBloc = context.read<QuizBloc>();
    quizBloc.add(NavigateToPrevious());
  }
}
