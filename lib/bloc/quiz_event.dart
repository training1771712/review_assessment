import 'package:equatable/equatable.dart';

abstract class QuizEvent extends Equatable {
  @override
  List<Object?> get props => [];
}

class FetchQuestions extends QuizEvent {}

class SetSelectedAnswer extends QuizEvent {
  final int? answerId;

  SetSelectedAnswer(this.answerId);

  @override
  List<Object?> get props => [answerId];
}

class NavigateToNext extends QuizEvent {}

class NavigateToPrevious extends QuizEvent {}
