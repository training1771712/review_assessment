import 'package:dio/dio.dart';
import 'package:final_task/bloc/quiz_state.dart';
import 'package:final_task/bloc/quiz_event.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class QuizBloc extends Bloc<QuizEvent, QuizState> {
  QuizBloc() : super(QuizInitial());

  Future<void> fetchQuestions() async {
    try {
      final dio = Dio();
      
      // Add the header
      dio.options.headers['Authorization'] = 'Bearer eyJhbGciOiJSUzI1NiIsImtpZCI6IkFFOEZERjQ1MjE0QTU4QUY3RkI5NTc1MEM3QTVFRDQyIiwidHlwIjoiYXQrand0In0.eyJuYmYiOjE2ODUwODEyODMsImV4cCI6MTY4NTE2NzY4MywiaXNzIjoiaHR0cHM6Ly91YXQuYWNjb3VudC5vbmVmaW4uYXBwIiwiY2xpZW50X2lkIjoiQ3VzdG9tZXJBcHBDbGllbnRDcmVkZW50aWFscyIsInN1YiI6ImFkZTJkYWYyLTY0YTgtNDUyZC04ZDFlLTRlNmM2YTdjMzFhZSIsImF1dGhfdGltZSI6MTY4NTA4MTI4MywiaWRwIjoibG9jYWwiLCJpYXQiOjE2ODUwODEyODMsInNjb3BlIjpbImN1c3RvbWVyLWxvZ2luIiwiY3VzdG9tZXItcmVnaXN0cmF0aW9uIiwibW9uZXktc2lnbiIsIm9mZmxpbmVfYWNjZXNzIl0sImFtciI6WyJwd2QiXX0.mpHdrdmmzDvrwXj3VuW9TyJfMZ5S7zLIyXbOvKYgN52f9LYww-Ful_1qBfCb_OH4qRee8jpXhMtDfNjxUOyEDT5wNYsJiQ7K92tjxci6BmfXX52tPyhJa2o0vuQTfW-1LLDkW_s_p1J7bqCOZ69nlGp37S_ve__2vIPNjmW4A0WJJfX4iRF7YoB3zmMoGsqUpoeyCQ50AiUTBP1pnwwrNJQOqHh9dZlLeYci4-PhpENeXkNx-UMsZ6XJQXZ_L1gX-D-DYWXzMXzqSHY4zvuHenKtgeH53tWc-2-MyYGCZaQxcS-W-Us98eCTx3NvD0k9bi9At3sm270GD-dZxiP_9A';

      //dio.interceptors.add(element);
      
      final response = await dio.get('https://uat.ms.onefin.app/api/user/money-sign-quiz');
      final responseData = response.data;

      if (responseData != null && responseData is Map<String, dynamic>) {
        if (response.statusCode == 200) {
          final moneySign = responseData['data']['money_sign'];

          if (moneySign != null && moneySign is List<dynamic>) {
            emit(QuizLoaded(moneySign));
          } else {
            throw Exception('Invalid data format: Money sign data is null or not a List<dynamic>');
          }
        } else {
          throw Exception('Failed to fetch questions from API');
        }
      } else {
        throw Exception('Invalid API response: Data is null or not a Map<String, dynamic>');
      }
    } catch (error) {
      throw Exception('Failed to fetch questions: $error');
    }
  }
}
