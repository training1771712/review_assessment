import 'package:equatable/equatable.dart';

abstract class QuizState extends Equatable {
  @override
  List<Object> get props => [];
}

class QuizInitial extends QuizState {}

class QuizLoading extends QuizState {}

class QuizLoaded extends QuizState {
  final List<dynamic> questions;

  QuizLoaded(this.questions);

  @override
  List<Object> get props => [questions];
}

class QuizError extends QuizState {
  final String errorMessage;

  QuizError(this.errorMessage);

  @override
  List<Object> get props => [errorMessage];
}
